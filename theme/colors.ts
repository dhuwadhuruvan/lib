const dark = {
  bgs1: "#333333",
  bgs2: "#414141",
  bgs3: "#565656",
  bgs4: "#696969",
  fgs1: "#F3F3F3",
  fgs2: "#CACACA",
  fgs3: "#D0D0D0",
  accent1: "#D6E5C2",
  accent2: "#ad6c6c",
};

const forest = {
  bgs1: "#2C3639",
  bgs2: "#3F4E4F",
  bgs3: "#596D73",
  bgs4: "#667D85",
  fgs1: "#DCD7C9",
  fgs2: "#CACACA",
  fgs3: "#D0D0D0",
  accent1: "#BF9E7C",
  accent2: "#ad6c6c",
};

const dracula = {
  bgs1: "#282A36",
  bgs2: "#343746",
  bgs3: "#565A73",
  bgs4: "#707594",
  fgs1: "#F9F9F9",
  fgs2: "#E6E6E6",
  fgs3: "#D0D0D0",
  accent1: "#A19BEC",
  accent2: "#ad6c6c",
};

const sea = {
  bgs1: "#0A2647",
  bgs2: "#144272",
  bgs3: "#205295",
  bgs4: "#2C74B3",
  fgs1: "#F3F3F3",
  fgs2: "#CACACA",
  fgs3: "#D0D0D0",
  accent1: "#59B2FF",
  accent2: "#dbcfc1",
};

const dim = {
  bgs1: "#404258",
  bgs2: "#474E68",
  bgs3: "#50577A",
  bgs4: "#6B728E",
  fgs1: "#F3F3F3",
  fgs2: "#CACACA",
  fgs3: "#D0D0D0",
  accent1: "#BAC1DB",
  accent2: "#dbcfc1",
};

const light = {
  bgs1: "#FCFCFC",
  bgs2: "#F2F2F2",
  bgs3: "#E6E6E6",
  bgs4: "#D9D9D9",
  fgs1: "#383838",
  fgs2: "#545454",
  fgs3: "#545454",
  accent1: "#A19BEC",
  accent2: "#ad6c6c",
};

const smoothy = {
  bgs1: "#F9F5E7",
  bgs2: "#F8EAD8",
  bgs3: "#EDDBC7",
  bgs4: "#E1C3A2",
  fgs1: "#003947",
  fgs2: "#003D4D",
  fgs3: "#003D4D",
  accent1: "#787168",
  accent2: "#d33682",
};

const grainy = {
  bgs1: "#FFFBEB",
  bgs2: "#ECE8DD",
  bgs3: "#E1D7C6",
  bgs4: "#E1D7C6",
  fgs1: "#003947",
  fgs2: "#003D4D",
  fgs3: "#003D4D",
  accent1: "#2aa198",
  accent2: "#cb4b16",
};

const solarized = {
  bgs1: "#fdf6e3",
  bgs2: "#eee8d5",
  bgs3: "#ddd6c1",
  bgs4: "#c9c3af",
  fgs1: "#073642",
  fgs2: "#6a868f",
  fgs3: "#7d9ea8",
  fgs4: "#91b8c4",
  accent1: "#268bd2",
  accent2: "#b58900",
};

export const colors = {
  dark,
  forest,
  dracula,
  sea,
  dim,
  light,
  smoothy,
  grainy,
  solarized,
};
